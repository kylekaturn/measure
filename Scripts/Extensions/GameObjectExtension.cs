using System.Collections.Generic;
using UnityEngine;

namespace Measure.Scripts.Extensions
{
	public static class GameObjectExtension
	{
		public static List<GameObject> GetChildren(this GameObject gameObject)
		{
			List<GameObject> ret = new List<GameObject>();
			gameObject.transform.GetChildren().ForEach(x =>
			{
				ret.Add(x.gameObject);
			});
			return ret;
		}

		public static bool HasComponent<T>(this GameObject target)
		{
			T component = target.GetComponent<T>();
			if (component == null) return false;
			return component.ToString() != "null";
		}

		public static bool HasAnyComponent(this GameObject target)
		{
			Component[] allComponents = target.GetComponents<Component>();
			return allComponents.Length > 1;
		}
	}
}