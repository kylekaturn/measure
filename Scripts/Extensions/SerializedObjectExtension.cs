using System.Collections.Generic;
using UnityEditor;

#if UNITY_EDITOR
namespace Measure.Scripts.Extensions
{
	public static class SerializedObjectExtension
	{
		public static List<SerializedProperty> GetSerializedProperties(this SerializedObject target)
		{
			List<SerializedProperty> serializedProperties = new List<SerializedProperty>();
			var iterator = target.GetIterator();
			if (iterator.NextVisible(true))
			{
				do
				{
					serializedProperties.Add(target.FindProperty(iterator.name));
				} while (iterator.NextVisible(false));
			}
			return serializedProperties;
		}
	}
}
#endif