using System;
using UnityEngine;

namespace Measure.Scripts.Extensions
{
	public static class RendererExtension
	{
		public static void SetMaterial(this Renderer target, Material material)
		{
			var materials = target.materials;
			Array.Fill(materials, material);
			target.materials = materials;
		}

		public static void SetMaterial(this Renderer target, Material material, int index)
		{
			var materials = target.materials;
			materials[index] = material;
			target.materials = materials;
		}
	}
}