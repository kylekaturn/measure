﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace Measure.Scripts.Extensions
{
	public static class StringExtension
	{
		public static bool IsNullOrEmpty(this string str) => string.IsNullOrEmpty(str);
		public static bool NotNullOrEmpty(this string str) => !string.IsNullOrEmpty(str);
		
		public static T AsEnum<T>(this string source, bool ignoreCase = true) where T : Enum => (T) Enum.Parse(typeof(T), source, ignoreCase);

		public static string RemoveFilenameFromPath(this string str)
		{
			return string.Join('/', str.Split('/').ToList().RemoveLast()) + "/";
		}
		
		public static string ReduceWhiteSpace(this string str)
		{
			Regex r = new Regex(@"\s+");
			return r.Replace(str, @" ");
		}

		public static bool IsImage(this string str)
		{
			return str.Contains("jpg") || str.Contains("png") || str.Contains("JPG") || str.Contains("PNG");
		}

		public static DateTime ToDateTime(this string str)
		{
			try
			{
				return Convert.ToDateTime(str);
			}
			catch
			{
				return DateTime.UtcNow;
			}
		}
	}
}
