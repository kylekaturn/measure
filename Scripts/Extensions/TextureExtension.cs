﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace Measure.Scripts.Extensions
{
	public static class TextureExtension
	{
		public enum TextureRotation { Left, Right, HalfCircle }
		public enum TextureFlipDirection { Vertical, Horizontal }

		public static void Merge(this Texture2D texture, Texture2D merge)
		{
			Color32[] pix1 = texture.GetPixels32();
			Color32[] pix2 = merge.GetPixels32();

			int x = 0;
			int y = 0;

			for (int j = 0; j < merge.height; j++)
			{
				for (int i = 0; i < merge.width; i++)
				{
					int a = texture.width / 2 - merge.width / 2 + x + i + texture.width * (texture.height / 2 - merge.height / 2 + j + y);
					int b = i + j * merge.width;

					pix1[a] = Color32.Lerp(pix1[a], pix2[b], pix2[b].a / 255.0f);
				}
			}
			texture.SetPixels32(pix1);
		}

		public static Texture2D GetReadableTexture(this Texture2D texture, Vector2Int resolution)
		{
			if (texture.isReadable) return texture;

			RenderTexture temporaryRT = RenderTexture.GetTemporary(resolution.x, resolution.y, 0);
			RenderTexture currentActiveRT = RenderTexture.active;
			RenderTexture.active = temporaryRT;

			Graphics.Blit(texture, temporaryRT);

			Texture2D newTexture = new Texture2D(resolution.x, resolution.y);
			newTexture.ReadPixels(new Rect(0, 0, temporaryRT.width, temporaryRT.height), 0, 0);
			newTexture.Apply();

			RenderTexture.active = currentActiveRT;
			RenderTexture.ReleaseTemporary(temporaryRT);
			return newTexture;
		}

		public static Texture2D Crop(this Texture2D texture, int x, int y, int x2, int y2)
		{
			int width = texture.width - x - x2;
			int height = texture.height - y - y2;
			Texture2D destTex = new Texture2D(width, height, texture.format, false);
			Color[] pix = texture.GetPixels(x, y, width, height);
			destTex.SetPixels(pix);
			destTex.Apply();
			return destTex;
		}

		public static Texture2D ChangeSize(this Texture2D texture2D, int targetX, int targetY)
		{
			RenderTexture rt = new RenderTexture(targetX, targetY, 24);
			RenderTexture.active = rt;
			Graphics.Blit(texture2D, rt);
			Texture2D result = new Texture2D(targetX, targetY);
			result.ReadPixels(new Rect(0, 0, targetX, targetY), 0, 0);
			result.Apply();
			return result;
		}

		public static void ReadFromBytes(this Texture2D texture, byte[] bytes)
		{
			Color32[] cols = new Color32[bytes.Length / 4];

			int count = 0;

			for (int y = texture.height - 1; y >= 0; y--)
			{
				for (int x = 0; x < texture.width; x++)
				{
					cols[x + y * texture.width] = new Color32(bytes[count + 2], bytes[count + 1], bytes[count + 0], bytes[count + 3]);
					count += 4;
				}
			}
			texture.SetPixels32(cols);
			texture.Apply();
		}

		public static void Flip(this Texture2D texture, TextureFlipDirection direction, bool apply = false)
		{
			Color32[] originalPixels = texture.GetPixels32();
			Color32[] flippedPixels = new Color32[originalPixels.Length];

			int count = 0;
			if (direction == TextureFlipDirection.Vertical)
			{
				for (int y = texture.height - 1; y >= 0; y--)
				{
					for (int x = 0; x < texture.width; x++)
					{
						flippedPixels[count++] = originalPixels[x + y * texture.width];
					}
				}
			}
			else
			{
				for (int y = 0; y < texture.height; y++)
				{
					for (int x = texture.width - 1; x >= 0; x--)
					{
						flippedPixels[count++] = originalPixels[x + y * texture.width];
					}
				}
			}
			texture.SetPixels32(flippedPixels);
			if (apply) texture.Apply();
		}

		public static void Rotate(this Texture2D texture, TextureRotation rotation, bool apply = false)
		{
			Color32[] originalPixels = texture.GetPixels32();
			IEnumerable<Color32> rotatedPixels;

			if (rotation == TextureRotation.HalfCircle)
				rotatedPixels = originalPixels.Reverse();
			else
			{
				int[] firstRowPixelIndeces = Enumerable.Range(0, texture.height).Select(i => i * texture.width).Reverse().ToArray();

				rotatedPixels = Enumerable.Repeat(firstRowPixelIndeces, texture.width).SelectMany(
					(frpi, rowIndex) => frpi.Select(i => originalPixels[i + rowIndex])
				);

				if (rotation == TextureRotation.Right) rotatedPixels = rotatedPixels.Reverse();
				texture.Reinitialize(texture.height, texture.width);
			}
			texture.SetPixels32(rotatedPixels.ToArray());
			if (apply) texture.Apply();
		}

		public static void Clear(this Texture2D texture, bool applyTexture = true)
		{
			texture.Fill(new Color32(0, 0, 0, 0), applyTexture);
		}

		public static void Fill(this Texture2D texture, Color32 color, bool applyTexture = true)
		{
			Color32[] colors = texture.GetPixels32();
			for (int i = 0; i < colors.Length; i++)
			{
				colors[i] = color;
			}
			texture.SetPixels32(colors);
			if (applyTexture) texture.Apply();
		}

		public static void SaveToPNG(this RenderTexture renderTexture, string filename)
		{
			RenderTexture activeRenderTexture = RenderTexture.active;
			RenderTexture.active = renderTexture;

			Texture2D texture = new Texture2D(renderTexture.width, renderTexture.height);
			texture.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
			texture.Apply();

			byte[] pngData = texture.EncodeToPNG();

			File.WriteAllBytes(filename, pngData);
			RenderTexture.active = activeRenderTexture;
		}

		public static void SaveToPNG(this Texture2D texture, string filename)
		{
			byte[] png = texture.EncodeToPNG();
			File.WriteAllBytes(filename, png);
		}

		public static void SaveToJPG(this Texture2D texture, string filename)
		{
			byte[] jpg = texture.EncodeToJPG(80);
			File.WriteAllBytes(filename, jpg);
		}

		public static Texture2D Clone(this Texture2D original)
		{
			Texture2D clone = new Texture2D(original.width, original.height);
			clone.SetPixels32(original.GetPixels32());
			clone.Apply();
			return clone;
		}

		public static float SetPixels32WithAlpha(this Texture2D texture, int x, int y, int blockWidth, int blockHeight, Color32[] colors)
		{
			Color32[] original = texture.GetPixels32();

			int i = x + (y * texture.width);
			int j = 0;

			while (i < original.Length && j < colors.Length)
			{
				if (i >= 0) original[i] = Color32.Lerp(original[i], colors[j], colors[j].a);
				i++;
				j++;
				if (j > 0 && j % blockWidth == 0)
				{
					i += texture.width - blockWidth;
				}
			}
			texture.SetPixels32(original);

			float ret = 0;
			for (x = 0; x < original.Length; x++)
			{
				ret += original[x].r;
			}
			return ret / original.Length;
		}
	}
}