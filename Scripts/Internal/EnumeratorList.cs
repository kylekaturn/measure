﻿using System.Collections.Generic;

namespace Measure.Scripts.Internal
{
	[System.Serializable]
	public class EnumeratorList<T> : List<T>
	{
		private int _currentIndex;

		public int currentIndex
		{
			get => _currentIndex;
			set
			{
				if (value < 0) value = Count - 1;
				if (value >= Count - 1) value = 0;
				_currentIndex = value;
			}
		}

		public T next
		{
			get
			{
				currentIndex++;
				return current;
			}
		}

		public T prev
		{
			get
			{
				currentIndex--;
				return current;
			}
		}

		public T current
		{
			get => this[currentIndex];
		}
	}
}
