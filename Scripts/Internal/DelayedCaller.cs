﻿using System.Collections.Generic;
using DG.Tweening;

namespace Measure.Scripts.Internal
{
    public static class DelayedCaller
    {
        private class DelayedCallerTween
        {
            public object refer;
            public Tween tween;
        }

        private static List<DelayedCallerTween> tweens = new List<DelayedCallerTween>();

        public static void DelayedCall(object refer, float time, TweenCallback callBack, bool killable = true, bool isLoop = false, bool ignoreTimeScale = false)
        {
            if (time < 0) time = 0;
            Tween tween = DOVirtual.DelayedCall(time, callBack, ignoreTimeScale).SetLoops(isLoop ? -1 : 0);
            if (killable)
            {
                tweens.Add(new DelayedCallerTween() { refer = refer, tween = tween });
            }
        }

        public static void DelayedCall(float time, TweenCallback callBack, bool killable = true, bool isLoop = false, bool ignoreTimeScale = false)
        {
            DelayedCall(tweens, time, callBack, killable, isLoop, ignoreTimeScale);
        }

        public static void KillOf(object refer)
        {
            for (int i = 0; i < tweens.Count; i++)
            {
                if (tweens[i].refer == refer)
                {
                    tweens[i].tween.Kill();
                    tweens.RemoveAt(i);
                    i--;
                }
            }
        }

        public static void KillEvery()
        {
            for (int i = 0; i < tweens.Count; i++)
            {
                tweens[i].tween.Kill();
                tweens.RemoveAt(i);
            }
        }

        public static void Kill(TweenCallback callBack)
        {
            for (int i = 0; i < tweens.Count; i++)
            {
                if (tweens[i].tween.onComplete == callBack)
                {
                    tweens[i].tween.Kill();
                    tweens.RemoveAt(i);
                }
            }
        }
    }
}
