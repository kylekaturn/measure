using Measure.Scripts.Attributes;
using Measure.Scripts.Extensions;
using Measure.Scripts.XR.Consoles;
using Measure.Scripts.XR.Controllers;
using Unity.XR.CoreUtils;
using Unity.XR.Oculus;
using UnityEngine;
using UnityEngine.Rendering.Universal;
using UnityEngine.XR;
using UnityEngine.XR.Management;
using Utils.XR;
using AppMetrics = Unity.XR.Oculus.Stats.AppMetrics;
using PerfMetrics = Unity.XR.Oculus.Stats.PerfMetrics;

namespace Measure.Scripts.XR
{
	[DefaultExecutionOrder(-3000)]
	public class XRPlayer : MonoBehaviour
	{
		public static XRPlayer Instance;

		[Separator("Configuration")]
		public XRConfiguration xrConfiguration;

		[Space(10)]
		[Header("Debug")]
		public bool debugHotkeyEnabled = true;
		[SerializeField] private bool _showGpuTimeConsole = false;
		public ConsoleHolder.Level consoleLevel = ConsoleHolder.Level.MonitorOnly;

		[Separator("Components")]
		[Label("XR Origin")] public XROrigin xrOrigin;
		[Label("Headset")] public Headset headset;
		public Controller controllerLeft;
		public Controller controllerRight;
		public QuestController questControllerLeft;
		public QuestController questControllerRight;
		public ConsoleHolder consoleHolder;

		public PlayerInputActions playerInputActions { get; private set; }
		public Controller GetController(ControllerType controllerType) => controllerType == ControllerType.Left ? controllerLeft : controllerRight;
		public Vector3 forwardDirection => Vector3.ProjectOnPlane(headset.transform.forward, transform.up).normalized;
		public float forwardAngle => Vector3.SignedAngle(transform.forward, forwardDirection, transform.up);
		public Vector3 headsetPosition => headset.transform.position;
		public Quaternion headsetRotation => headset.transform.rotation;
		public int appGPUTime => platform == XRPlatform.Device ? (int) (PerfMetrics.AppGPUTime * 1000000) : 0;
		public Vector2 eyeTextureSize => platform == XRPlatform.Device ? new Vector2(XRSettings.eyeTextureWidth, XRSettings.eyeTextureHeight) : Vector2.one;
		
		private ControllerType _primaryControllerType = ControllerType.Right;
		private Controller _primaryController => _primaryControllerType == ControllerType.Left ? controllerLeft : controllerRight;
		private Controller _secondaryController => _primaryControllerType == ControllerType.Left ? controllerRight : controllerLeft;
		private XRGeneralSettings _xrSettings;
		private XRManagerSettings _xrManager;
		private XRLoader _xrLoader;
		private XRInputSubsystem _xrInputSystem;

		public static XRPlatform platform
		{
			get
			{
				return Application.isEditor switch
				{
					true when XRDevice.refreshRate == 0 => XRPlatform.Editor,
					true when XRDevice.refreshRate > 0 => XRPlatform.OculusLink,
					_ => XRPlatform.Device
				};
			}
		}

		public bool ShowGPUTimeConsole
		{
			get => _showGpuTimeConsole;
			set
			{
				_showGpuTimeConsole = value;
				if (value)
				{
					consoleHolder.gpuTimeConsole.Show();
				}
				else
				{
					consoleHolder.gpuTimeConsole.Hide();
				}
			}
		}



		protected virtual void Awake()
		{
			if (Instance != null)
			{
				Instance.Setup(xrConfiguration);
				Instance.transform.position = transform.position;
				Instance.transform.rotation = transform.rotation;
				DestroyImmediate(gameObject);
			}
			else
			{
				Instance = this;
			}
		}

		protected virtual void Start()
		{
			Application.targetFrameRate = 72;
			DontDestroyOnLoad(gameObject);
			transform.SetParent(null);

			playerInputActions = new PlayerInputActions();
			playerInputActions.Enable();

			AppMetrics.EnableAppMetrics(true);
			PerfMetrics.EnablePerfMetrics(true);

			Debug.Log($"Identifier : {Application.identifier}");
			Debug.Log($"Version : {Application.version}");
			Debug.Log($"Unity Version : {Application.unityVersion}");
			Debug.Log($"Platform : {Application.platform}");
			Debug.Log($"Graphics API : {SystemInfo.graphicsDeviceType}");
			Debug.Log($"Color Space : {QualitySettings.activeColorSpace}");
			Debug.Log($"Refresh Rate : {XRDevice.refreshRate}");
			//Debug.Log($"Foveated Rendering Supported : {Unity.XR.Oculus.Utils.eyeTrackedFoveatedRenderingSupported}");

			consoleHolder.level = consoleLevel;
			if (_showGpuTimeConsole) consoleHolder.gpuTimeConsole.Show();
			Setup(xrConfiguration);

			controllerRight.JoystickButtonShort.performed += context =>
			{
				if (debugHotkeyEnabled) consoleLevel = consoleHolder.nextLevel;
			};
			controllerRight.JoystickButtonLong.performed += context =>
			{
				consoleHolder.gpuTimeConsole.Toggle();
			};
			controllerRight.Turn.performed += context =>
			{
				if (xrConfiguration.turnEnabled) xrOrigin.RotateAroundCameraUsingOriginUp(xrConfiguration.turnAngle * context.ReadValue<Vector2>().x.ToInt());
			};
		}

		public virtual void Setup(XRConfiguration configuration)
		{
			xrConfiguration = configuration;
			showQuestController = xrConfiguration.showQuestController;
			uiInteractorEnabled = xrConfiguration.uiInteractorEnabled;
			renderingMode = xrConfiguration.renderingMode;
			depthPrimingMode = xrConfiguration.depthPrimingMode;
			copyDepthMode = xrConfiguration.copyDepthMode;
			useNativeRenderpass = xrConfiguration.useNativeRenderpass;
			intermediateTextureMode = xrConfiguration.intermediateTextureMode;
			rendererFeature = xrConfiguration.rendererFeature;
			depthTexture = xrConfiguration.depthTexture;
			opaqueTexture = xrConfiguration.opaqueTextue;
			shadow = xrConfiguration.shadow;
			skybox = xrConfiguration.skybox;
			postProcessing = xrConfiguration.postProcessing;
			hdr = xrConfiguration.hdr;
			bloom = xrConfiguration.bloom;
			bloomIntensity = xrConfiguration.bloomIntensity;
			bloomScatter = xrConfiguration.bloomScatter;
			bloomThreshold = xrConfiguration.bloomThreshold;
			bloomMaxIteration = xrConfiguration.bloomMaxIteration;
			antialiasingType = xrConfiguration.antialiasingType;
			msaa = xrConfiguration.msaa;
			renderScale = xrConfiguration.renderScale;
			ffrLevel = xrConfiguration.ffrLevel;
			cpuLevel = xrConfiguration.cpuLevel;
			gpuLevel = xrConfiguration.gpuLevel;
			simulatorHeight = xrConfiguration.simualatorHeight;
		}

		protected virtual void Update()
		{
			if (xrConfiguration.translateEnabled) Translate();
			consoleHolder.level = consoleLevel;
			uiInteractorEnabled = xrConfiguration.uiInteractorEnabled;
		}

		private void Translate()
		{
			if (xrConfiguration.translateEnabled)
			{
				Vector2 joystickValue = controllerLeft.GetJoystickValue();
				Vector3 translate = new Vector3(joystickValue.x, 0, joystickValue.y) * xrConfiguration.translationSpeed * Time.deltaTime;
				Vector3 final = Quaternion.AngleAxis(forwardAngle, transform.up) * translate;
				transform.Translate(final);
			}
		}

		protected virtual void OnApplicationFocus(bool hasFocus)
		{
			//Debug.Log("OnApplicationFocus : " + hasFocus);
			if (platform == XRPlatform.Editor) return;
			if (hasFocus)
			{
				controllerLeft.gameObject.SetActive(true);
				controllerRight.gameObject.SetActive(true);
			}
			else
			{
				controllerLeft.gameObject.SetActive(false);
				controllerRight.gameObject.SetActive(false);
			}
		}


		//Configuration 
		public bool showQuestController
		{
			get => xrConfiguration.showQuestController;
			set
			{
				xrConfiguration.showQuestController = value;
				questControllerLeft.gameObject.SetActive(value);
				questControllerRight.gameObject.SetActive(value);
			}
		}

		public bool uiInteractorEnabled
		{
			get => xrConfiguration.uiInteractorEnabled;
			set
			{
				xrConfiguration.uiInteractorEnabled = value;
				if (value)
				{
					_primaryController.uiInteractorEnabled = true;
					_secondaryController.uiInteractorEnabled = false;
				}
				else
				{
					_primaryController.uiInteractorEnabled = false;
					_secondaryController.uiInteractorEnabled = false;
				}
			}
		}

		public bool autoSwitchUIInteractor
		{
			get => xrConfiguration.autoSwitchUIInteractor;
			set => xrConfiguration.autoSwitchUIInteractor = value;
		}

		public RenderingMode renderingMode
		{
			get => xrConfiguration.renderingMode;
			set => xrConfiguration.renderingMode = headset.xrCamera.renderingMode = value;
		}

		public DepthPrimingMode depthPrimingMode
		{
			get => xrConfiguration.depthPrimingMode;
			set => xrConfiguration.depthPrimingMode = headset.xrCamera.depthPrimingMode = value;
		}

		public CopyDepthMode copyDepthMode
		{
			get => xrConfiguration.copyDepthMode;
			set => xrConfiguration.copyDepthMode = headset.xrCamera.copyDepthMode = value;
		}

		public bool useNativeRenderpass
		{
			get => xrConfiguration.useNativeRenderpass;
			set => xrConfiguration.useNativeRenderpass = headset.xrCamera.useNativeRenderpass = value;
		}

		public IntermediateTextureMode intermediateTextureMode
		{
			get => xrConfiguration.intermediateTextureMode;
			set => xrConfiguration.intermediateTextureMode = headset.xrCamera.intermediateTextureMode = value;
		}

		public bool rendererFeature
		{
			get => xrConfiguration.rendererFeature;
			set => xrConfiguration.rendererFeature = headset.xrCamera.rendererFeature = value;
		}

		public bool depthTexture
		{
			get => xrConfiguration.depthTexture;
			set => xrConfiguration.depthTexture = headset.xrCamera.depthTexture = value;
		}

		public bool opaqueTexture
		{
			get => xrConfiguration.opaqueTextue;
			set => xrConfiguration.opaqueTextue = headset.xrCamera.opaqueTexture = value;
		}

		public bool shadow
		{
			get => xrConfiguration.shadow;
			set => xrConfiguration.shadow = headset.xrCamera.shadow = value;
		}

		public bool skybox
		{
			get => xrConfiguration.skybox;
			set => xrConfiguration.skybox = headset.xrCamera.skybox = value;
		}

		public bool postProcessing
		{
			get => xrConfiguration.postProcessing;
			set => xrConfiguration.postProcessing = headset.xrCamera.postProcessing = value;
		}

		public bool hdr
		{
			get => xrConfiguration.hdr;
			set => xrConfiguration.hdr = headset.xrCamera.hdr = value;
		}

		public bool bloom
		{
			get => xrConfiguration.bloom;
			set => xrConfiguration.bloom = headset.xrCamera.bloom = value;
		}

		public float bloomThreshold
		{
			get => xrConfiguration.bloomThreshold;
			set => xrConfiguration.bloomThreshold = headset.xrCamera.bloomThreshold = value;
		}

		public float bloomIntensity
		{
			get => xrConfiguration.bloomIntensity;
			set => xrConfiguration.bloomIntensity = headset.xrCamera.bloomItensity = value;
		}

		public float bloomScatter
		{
			get => xrConfiguration.bloomScatter;
			set => xrConfiguration.bloomScatter = headset.xrCamera.bloomScatter = value;
		}

		public int bloomMaxIteration
		{
			get => xrConfiguration.bloomMaxIteration;
			set => xrConfiguration.bloomMaxIteration = headset.xrCamera.bloomMaxIteration = value;
		}

		public AntialiasingType antialiasingType
		{
			get => xrConfiguration.antialiasingType;
			set => xrConfiguration.antialiasingType = headset.xrCamera.antialiasingType = value;
		}

		public MsaaQuality msaa
		{
			get => xrConfiguration.msaa;
			set => xrConfiguration.msaa = headset.xrCamera.msaa = value;
		}

		public float renderScale
		{
			get => xrConfiguration.renderScale;
			set
			{
				xrConfiguration.renderScale = value;
				headset.xrCamera.renderScale = value;
			}
		}

		public FFRLevel ffrLevel
		{
			get => xrConfiguration.ffrLevel;
			set
			{
				xrConfiguration.ffrLevel = value;
				Unity.XR.Oculus.Utils.foveatedRenderingLevel = ((int) value);
			}
		}

		public ProcessorPerformanceLevel cpuLevel
		{
			get => xrConfiguration.cpuLevel;
			set
			{
				xrConfiguration.cpuLevel = value;
				Performance.TrySetCPULevel((int) value);
			}
		}

		public ProcessorPerformanceLevel gpuLevel
		{
			get => xrConfiguration.gpuLevel;
			set
			{
				xrConfiguration.cpuLevel = value;
				Performance.TrySetGPULevel((int) value);
			}
		}

		public float simulatorHeight
		{
			get => xrConfiguration.simualatorHeight;
			set => xrConfiguration.simualatorHeight = value;
		}
	}
}