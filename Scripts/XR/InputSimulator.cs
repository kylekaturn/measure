using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.InputSystem.LowLevel;
using UnityEngine.Serialization;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation;
using InputSystem = UnityEngine.InputSystem;
using Random = UnityEngine.Random;

namespace Measure.Scripts.XR
{
	public class InputSimulator : MonoBehaviour
	{
		public static InputSimulator Instance;
		private enum TrackingMode { HMD, LeftController, RightController }

		[FormerlySerializedAs("primaryTrackingMode")] [SerializeField] private TrackingMode _primaryTrackingMode = TrackingMode.HMD;

		private XRSimulatedHMDState _hmdState;
		private XRSimulatedControllerState _leftControllerState;
		private XRSimulatedControllerState _rightControllerState;

		private XRSimulatedHMD _hmdDevice;
		private XRSimulatedController _leftControllerDevice;
		private XRSimulatedController _rightControllerDevice;

		private TrackingMode _trackingMode = TrackingMode.HMD;
		private XRPlayer _xrPlayer;
		private float _defaultHeight = 1.5f;

		[NonSerialized] public Vector3 hmdRotation = new Vector3(0, 0, 0);
		[NonSerialized] public Vector3 leftControllerPosition = new Vector3(-0.16f, -0.1f, 0.4f);
		[NonSerialized] public Vector3 rightControllerPosition = new Vector3(0.16f, -0.1f, 0.4f);
		[NonSerialized] public Vector3 leftControllerRotation = new Vector3(0, 0, 0);
		[NonSerialized] public Vector3 rightControllerRotation = new Vector3(0, 0, 0);
		[NonSerialized] public Vector3 leftControllerOffset = new Vector3(0, 0, 0);
		[NonSerialized] public Vector3 rightControllerOffset = new Vector3(0, 0, 0);

		private void Awake()
		{
			if (XRPlayer.platform is XRPlatform.OculusLink or XRPlatform.Device)
			{
				Instance = null;
				Destroy(gameObject);
				return;
			}

			Instance = this;
			Cursor.lockState = CursorLockMode.Locked;
			_xrPlayer = XRPlayer.Instance;
			_defaultHeight = _xrPlayer.simulatorHeight;
			_hmdState.Reset();
			_leftControllerState.Reset();
			_rightControllerState.Reset();
		}

		private void OnEnable()
		{
			AddDevices();
		}

		private void OnDisable()
		{
			RemoveDevices();
		}

		private void Update()
		{
			switch (Cursor.lockState)
			{
				case CursorLockMode.None:
					if (Input.GetMouseButtonDown(0)) Cursor.lockState = CursorLockMode.Locked;
					break;
				case CursorLockMode.Locked:
					if (Input.GetKeyDown(KeyCode.Escape)) Cursor.lockState = CursorLockMode.None;
					break;
			}
			if (Cursor.lockState == CursorLockMode.None) return;

			XRPlayer.Instance.autoSwitchUIInteractor = false;

			Vector2 mouseDelta = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y")) * (Application.platform == RuntimePlatform.WindowsEditor ? 0.5f : 6f);
			bool mouseLeftButtonPressed = Input.GetMouseButton(0);
			bool mouseRightButtonPressed = Input.GetMouseButton(1);
			bool mouseWheelButtonPressed = Input.GetMouseButton(2) || Input.GetKey(KeyCode.BackQuote);
			bool mouseBackButtonPressed = Input.GetKey(KeyCode.Mouse3);
			bool mouseForwardButtonPressed = Input.GetKey(KeyCode.Mouse4);
			bool enterKeyPressed = Input.GetKey(KeyCode.Return);
			bool backspaceKeyPressed = Input.GetKey(KeyCode.Backspace);
			Vector2 wasdKey = new Vector2(0, 0);
			if (Input.GetKey(KeyCode.A)) wasdKey.x = -1;
			if (Input.GetKey(KeyCode.D)) wasdKey.x = 1;
			if (Input.GetKey(KeyCode.W)) wasdKey.y = 1;
			if (Input.GetKey(KeyCode.S)) wasdKey.y = -1;
			Vector2 arrowKey = new Vector2(0, 0);
			if (Input.GetKey(KeyCode.LeftArrow)) arrowKey.x = -1;
			if (Input.GetKey(KeyCode.RightArrow)) arrowKey.x = 1;
			if (Input.GetKey(KeyCode.UpArrow)) arrowKey.y = 1;
			if (Input.GetKey(KeyCode.DownArrow)) arrowKey.y = -1;

			if (Input.GetKey(KeyCode.Z))
			{
				_trackingMode = TrackingMode.LeftController;
			}
			else if (Input.GetKey(KeyCode.X))
			{
				_trackingMode = TrackingMode.RightController;
			}
			else
			{
				_trackingMode = _primaryTrackingMode;
			}

			switch (_trackingMode)
			{
				case TrackingMode.HMD:
					hmdRotation += new Vector3(-mouseDelta.y, mouseDelta.x, 0);
					_rightControllerState.primary2DAxis = arrowKey;
					goto rightControllerWithoutPosition;

				case TrackingMode.LeftController:
					leftControllerPosition += new Vector3(mouseDelta.x, mouseDelta.y, 0) * 0.001f;
					leftControllerPosition += new Vector3(arrowKey.x, 0f, arrowKey.y) * Time.deltaTime * 0.5f;
					leftControllerRotation += new Vector3(-mouseDelta.y, mouseDelta.x, 0) * 0.5f;
					_leftControllerState.trigger = mouseLeftButtonPressed ? 1f : 0f;
					_leftControllerState.grip = mouseRightButtonPressed ? 1f : 0f;
					_leftControllerState.WithButton(ControllerButton.TriggerButton, mouseLeftButtonPressed);
					_leftControllerState.WithButton(ControllerButton.GripButton, mouseRightButtonPressed);
					_leftControllerState.WithButton(ControllerButton.PrimaryButton, enterKeyPressed);
					_leftControllerState.WithButton(ControllerButton.SecondaryButton, backspaceKeyPressed);
					_leftControllerState.WithButton(ControllerButton.Primary2DAxisClick, mouseWheelButtonPressed);
					goto default;

				case TrackingMode.RightController:
					rightControllerPosition += new Vector3(mouseDelta.x, mouseDelta.y, 0) * 0.001f;
					rightControllerPosition += new Vector3(arrowKey.x, 0f, arrowKey.y) * Time.deltaTime * 0.5f;
					rightControllerRotation += new Vector3(-mouseDelta.y, mouseDelta.x, 0) * 0.5f;
					rightControllerWithoutPosition:
					_rightControllerState.trigger = mouseLeftButtonPressed ? 1f : 0f;
					_rightControllerState.grip = mouseRightButtonPressed ? 1f : 0f;
					_rightControllerState.WithButton(ControllerButton.TriggerButton, mouseLeftButtonPressed);
					_rightControllerState.WithButton(ControllerButton.GripButton, mouseRightButtonPressed);
					_rightControllerState.WithButton(ControllerButton.PrimaryButton, enterKeyPressed);
					_rightControllerState.WithButton(ControllerButton.SecondaryButton, backspaceKeyPressed);
					_rightControllerState.WithButton(ControllerButton.Primary2DAxisClick, mouseWheelButtonPressed);
					goto default;

				default:
					_leftControllerState.primary2DAxis = wasdKey;
					break;
			}

			_hmdState.isTracked = true;
			_hmdState.trackingState = (int) (InputTrackingState.Position | InputTrackingState.Rotation);
			_hmdState.centerEyePosition = new Vector3(0f, _defaultHeight, 0f) + Random.onUnitSphere * 0.0001f;
			_hmdState.devicePosition = _hmdState.centerEyePosition;
			_hmdState.centerEyeRotation = Quaternion.Euler(hmdRotation + Random.onUnitSphere * 0.0001f);
			_hmdState.deviceRotation = _hmdState.centerEyeRotation;
			_xrPlayer.headset.transform.localRotation = _hmdState.deviceRotation;

			_leftControllerState.isTracked = true;
			_leftControllerState.trackingState = (int) (InputTrackingState.Position | InputTrackingState.Rotation);
			_leftControllerState.devicePosition = _xrPlayer.transform.InverseTransformPoint(_xrPlayer.headset.transform.TransformPoint(leftControllerPosition)) + (Random.onUnitSphere * 0.0001f);
			_leftControllerState.devicePosition += _xrPlayer.controllerLeft.transform.TransformVector(leftControllerOffset);
			_leftControllerState.deviceRotation = Quaternion.Euler(leftControllerRotation + _xrPlayer.headset.transform.eulerAngles - _xrPlayer.transform.eulerAngles + (Random.onUnitSphere * 0.0001f));

			_rightControllerState.isTracked = true;
			_rightControllerState.trackingState = (int) (InputTrackingState.Position | InputTrackingState.Rotation);
			_rightControllerState.devicePosition = _xrPlayer.transform.InverseTransformPoint(_xrPlayer.headset.transform.TransformPoint(rightControllerPosition)) + (Random.onUnitSphere * 0.0001f);
			_rightControllerState.devicePosition += _xrPlayer.controllerRight.transform.TransformVector(rightControllerOffset);
			_rightControllerState.deviceRotation = Quaternion.Euler(rightControllerRotation + _xrPlayer.headset.transform.eulerAngles - _xrPlayer.transform.eulerAngles + (Random.onUnitSphere * 0.0001f));

			if (_hmdDevice != null)
			{
				InputState.Change(_hmdDevice, _hmdState);
				InputState.Change(_leftControllerDevice, _leftControllerState);
				InputState.Change(_rightControllerDevice, _rightControllerState);
			}
		}

		private void AddDevices()
		{
			_hmdDevice = InputSystem.InputSystem.AddDevice<XRSimulatedHMD>();
			if (_hmdDevice == null)
			{
				Debug.LogError($"Failed to create {nameof(XRSimulatedHMD)}.");
			}

			_leftControllerDevice = InputSystem.InputSystem.AddDevice<XRSimulatedController>($"{nameof(XRSimulatedController)} - {InputSystem.CommonUsages.LeftHand}");
			if (_leftControllerDevice != null)
			{
				InputSystem.InputSystem.SetDeviceUsage(_leftControllerDevice, InputSystem.CommonUsages.LeftHand);
			}
			else
			{
				Debug.LogError($"Failed to create {nameof(XRSimulatedController)} for {InputSystem.CommonUsages.LeftHand}.", this);
			}

			_rightControllerDevice = InputSystem.InputSystem.AddDevice<XRSimulatedController>($"{nameof(XRSimulatedController)} - {InputSystem.CommonUsages.RightHand}");
			if (_rightControllerDevice != null)
			{
				InputSystem.InputSystem.SetDeviceUsage(_rightControllerDevice, InputSystem.CommonUsages.RightHand);
			}
			else
			{
				Debug.LogError($"Failed to create {nameof(XRSimulatedController)} for {InputSystem.CommonUsages.RightHand}.", this);
			}
		}

		private void RemoveDevices()
		{
			if (_hmdDevice is {added: true}) InputSystem.InputSystem.RemoveDevice(_hmdDevice);
			if (_leftControllerDevice is {added: true}) InputSystem.InputSystem.RemoveDevice(_leftControllerDevice);
			if (_rightControllerDevice is {added: true}) InputSystem.InputSystem.RemoveDevice(_rightControllerDevice);
		}
	}

	public static class InputSimulatorExtension
	{
		public static Tweener DoLeftControllerOffset(this InputSimulator target, Vector3 endValue, float duration)
		{
			return DOTween.To(() => target.leftControllerOffset, (x) => target.leftControllerOffset = x, endValue, duration);
		}

		public static Tweener DoRightControllerOffset(this InputSimulator target, Vector3 endValue, float duration)
		{
			return DOTween.To(() => target.rightControllerOffset, (x) => target.rightControllerOffset = x, endValue, duration);
		}

		public static void PunchStraightLeft(this InputSimulator target, float duration = 0.1f)
		{
			target.DoLeftControllerOffset(new Vector3(0.2f, 0.13f, 0.65f), duration).SetEase(Ease.InSine);
			target.DoLeftControllerOffset(Vector3.zero, duration).SetEase(Ease.OutSine).SetDelay(duration);
		}

		public static void PunchStraightRight(this InputSimulator target, float duration = 0.1f)
		{
			target.DoRightControllerOffset(new Vector3(-0.2f, 0.13f, 0.65f), duration).SetEase(Ease.InSine);
			target.DoRightControllerOffset(Vector3.zero, duration).SetEase(Ease.OutSine).SetDelay(duration);
		}
	}
}