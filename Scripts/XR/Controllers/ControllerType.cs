namespace Measure.Scripts.XR.Controllers
{
	public enum ControllerType
	{
		Left = -1,
		Right = 1
	}
}