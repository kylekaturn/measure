using Measure.Scripts.Extensions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.XR.Interaction.Toolkit.UI;

namespace Measure.Scripts.XR.Controllers
{
	public class UIInteractor : MonoBehaviour, IUIInteractor
	{
		[Header("UI Interactor")]
		public GameObject ray;
		public GameObject cursor;
		public LayerMask raycastMask = -1;
		public float rayDistance = 10f;
		public float drawDistance = 1f;
		public bool uiSelect = false;

		private XRUIInputModule _inputModule;
		private Material _rayMaterial;
		private int _matcapEnabledID = Shader.PropertyToID("_MatcapEnabled");
		private int _gradationEnabledID = Shader.PropertyToID("_GradationEnabled");
		private int _intensityID = Shader.PropertyToID("_Intensity");

		private void Awake()
		{
			_rayMaterial = ray.GetComponent<MeshRenderer>().material;
		}

		private void OnEnable()
		{
			var eventSystem = FindFirstObjectByType<EventSystem>();
			_inputModule = eventSystem.GetComponent<XRUIInputModule>();
			_inputModule.RegisterInteractor(this);
		}

		private void OnDisable()
		{
			_inputModule.UnregisterInteractor(this);
		}

		public bool isHittingUI { get; private set; } = false;

		private void Update()
		{
			if (TryGetCurrentUIRaycastResult(out RaycastResult result, out int index))
			{
				cursor.SetActive(true);
				ray.SetActive(true);
				cursor.transform.eulerAngles = result.worldNormal;
				cursor.transform.position = result.worldPosition;
				ray.transform.SetScaleZ(Vector3.Distance(transform.position, result.worldPosition));
				gradationEnabled = false;
				intensity = 3f;
				isHittingUI = true;
			}
			else
			{
				cursor.SetActive(false);
				if (drawDistance == 0) ray.SetActive(false);
				ray.transform.SetScaleZ(drawDistance);
				gradationEnabled = true;
				intensity = 1.5f;
				isHittingUI = false;
			}
		}

		public bool TryGetUIModel(out TrackedDeviceModel model)
		{
			if (_inputModule != null)
			{
				return _inputModule.GetTrackedDeviceModel(this, out model);
			}

			model = new TrackedDeviceModel(-1);
			return false;
		}

		public void UpdateUIModel(ref TrackedDeviceModel model)
		{
			model.position = transform.position;
			model.orientation = transform.rotation;
			model.select = uiSelect;
			model.raycastLayerMask = raycastMask;

			var raycastPoints = model.raycastPoints;
			raycastPoints.Clear();
			raycastPoints.Capacity = 2;
			raycastPoints.Add(transform.position);
			raycastPoints.Add(transform.position + transform.forward * rayDistance);
		}

		public bool TryGetCurrentUIRaycastResult(out RaycastResult raycastResult, out int raycastEndpointIndex)
		{
			if (TryGetUIModel(out var model) && model.currentRaycast.isValid)
			{
				raycastResult = model.currentRaycast;
				raycastEndpointIndex = model.currentRaycastEndpointIndex;
				return true;
			}

			raycastResult = default;
			raycastEndpointIndex = default;
			return false;
		}

		private bool matcapEnabled
		{
			get => _rayMaterial.GetFloat(_matcapEnabledID).ToBool();
			set => _rayMaterial.SetFloat(_matcapEnabledID, value.ToFloat());
		}

		private bool gradationEnabled
		{
			get => _rayMaterial.GetFloat(_gradationEnabledID).ToBool();
			set => _rayMaterial.SetFloat(_gradationEnabledID, value.ToFloat());
		}

		private float intensity
		{
			get => _rayMaterial.GetFloat(_intensityID);
			set => _rayMaterial.SetFloat(_intensityID, value);
		}
	}
}