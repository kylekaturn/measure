using TMPro;
using UnityEngine;

namespace Measure.Scripts.XR.Consoles
{
	public class GPUTimeConsole : MonoBehaviour
	{
		public TextMeshProUGUI text;

		private void Awake()
		{
			Hide();
		}

		public void Show()
		{
			gameObject.SetActive(true);
		}

		public void Hide()
		{
			gameObject.SetActive(false);
		}

		public void Toggle()
		{
			gameObject.SetActive(!gameObject.activeSelf);
		}

		private void Update()
		{
			text.SetText(XRPlayer.Instance.appGPUTime.ToString());
		}
	}
}