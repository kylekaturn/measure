using System;
using Measure.Scripts.Extensions;
using Measure.Scripts.Internal;
using UnityEngine;
using UnityEngine.UI;

namespace Measure.Scripts.XR.Consoles
{
	public class LogConsole : MonoBehaviour
	{
		public Image background;
		public bool autoHide = true;
		public Action OnHide;

		private Vector3 _defaultPosition;

		private void Awake()
		{
			background.gameObject.SetActive(false);
			_defaultPosition = transform.position;
		}

		private void Start()
		{
			//if (autoHide) debugLogManager.HideLogWindow();
		}

		public void Show()
		{
			transform.position = XRPlayer.Instance.transform.TransformPoint(_defaultPosition);
			transform.rotation = Quaternion.LookRotation(transform.position.Floor() - XRPlayer.Instance.transform.position);
			//debugLogManager.ShowLogWindow();
			//curvedUICanvas.enabled = true;
		}

		public void Hide()
		{
			//debugLogManager.HideLogWindow();
			DelayedCaller.DelayedCall(0.1f, () =>
			{
				//curvedUICanvas.enabled = false;
			});
		}
	}
}