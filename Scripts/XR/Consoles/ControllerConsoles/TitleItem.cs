using TMPro;
using UnityEngine;

namespace Measure.Scripts.XR.Consoles.ControllerConsoles
{
	public class TitleItem : MonoBehaviour
	{
		public TextMeshProUGUI label;

		public void Setup(string title)
		{
			label.text = title;
		}
	}
}