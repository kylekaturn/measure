using TMPro;
using UnityEngine;

namespace Measure.Scripts.XR.Consoles.ControllerConsoles
{
	public class LabelItem : MonoBehaviour
	{
		public TextMeshProUGUI label;

		public void Setup(string title)
		{
			label.SetText(title);
		}

		public void SetText(string text)
		{
			label.SetText(text);
		}
	}
}