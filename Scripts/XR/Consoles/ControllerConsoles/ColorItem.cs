using System;
using Measure.Scripts.Extensions;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Measure.Scripts.XR.Consoles.ControllerConsoles
{
	public class ColorItem : MonoBehaviour
	{
		public TextMeshProUGUI label;
		public Slider rSlider;
		public Slider gSlider;
		public Slider bSlider;
		public Image preview;

		public void Setup(string title, Color defaultValue, Action<Color> OnValueChanged)
		{
			label.text = $"{title} : {defaultValue.ToHexString()}";
			preview.color = defaultValue;
			rSlider.value = defaultValue.r;
			gSlider.value = defaultValue.g;
			bSlider.value = defaultValue.b;
			rSlider.onValueChanged.AddListener(value =>
			{
				Color newColor = new Color(rSlider.value, gSlider.value, bSlider.value);
				preview.color = newColor;
				label.text = $"{title} : {newColor.ToHexString()}";
				OnValueChanged(newColor);
			});
			gSlider.onValueChanged.AddListener(value =>
			{
				Color newColor = new Color(rSlider.value, gSlider.value, bSlider.value);
				preview.color = newColor;
				label.text = $"{title} : {newColor.ToHexString()}";
				OnValueChanged(newColor);
			});
			bSlider.onValueChanged.AddListener(value =>
			{
				Color newColor = new Color(rSlider.value, gSlider.value, bSlider.value);
				preview.color = newColor;
				label.text = $"{title} : {newColor.ToHexString()}";
				OnValueChanged(newColor);
			});
		}
	}
}