using Measure.Scripts.Extensions;
using UnityEngine;
using UnityEngine.UI;

namespace Measure.Scripts.XR.Consoles.ControllerConsoles
{
	public class ControllerConsole : MonoBehaviour
	{
		[Header("Item Prefabs")]
		[SerializeField] private ToggleItem _toggleItemPrefab;
		[SerializeField] private ToggleGroupItem _toggleGroupItemPrefab;
		[SerializeField] private ColorItem _colorItemPrefab;
		[SerializeField] private EnumItem _enumItemPrefab;
		[SerializeField] private EnumItem _enumVerticalItemPrefab;
		[SerializeField] private NumberItem _numberItemPrefab;
		[SerializeField] private SpacerItem _spacerItemPrefab;
		[SerializeField] private ButtonItem _buttonItemPrefab;
		[SerializeField] private TitleItem _titleItemPrefab;
		[SerializeField] private LabelItem _labelItemPrefab;

		[Header("Components")]
		[SerializeField] private Transform _holder;
		[SerializeField] private ContentSizeFitter _contentSizeFitter;

		public ToggleItem AddToggleItem() => Instantiate(_toggleItemPrefab, _holder);
		public ToggleGroupItem AddToggleGroupItem() => Instantiate(_toggleGroupItemPrefab, _holder);
		public ColorItem AddColorItem() => Instantiate(_colorItemPrefab, _holder);
		public EnumItem AddEnumItem() => Instantiate(_enumItemPrefab, _holder);
		public EnumItem AddEnumVerticalItem() => Instantiate(_enumVerticalItemPrefab, _holder);
		public NumberItem AddNumerItem() => Instantiate(_numberItemPrefab, _holder);
		public SpacerItem AddSpacerItem() => Instantiate(_spacerItemPrefab, _holder);
		public ButtonItem AddButtonItem() => Instantiate(_buttonItemPrefab, _holder);
		public TitleItem AddTitleItem() => Instantiate(_titleItemPrefab, _holder);
		public LabelItem AddLabelItem() => Instantiate(_labelItemPrefab, _holder);

		public void Reset()
		{
			_holder.DestroyChildren();
		}

		public bool autoSize
		{
			get => _contentSizeFitter.enabled;
			set => _contentSizeFitter.enabled = value;
		}
	}
}