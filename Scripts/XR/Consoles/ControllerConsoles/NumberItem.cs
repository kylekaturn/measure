using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Measure.Scripts.XR.Consoles.ControllerConsoles
{
	public class NumberItem : MonoBehaviour
	{
		public TextMeshProUGUI label;
		public Slider slider;
		private string _title = "";
		private string _format = "F0";

		private void Awake()
		{
			slider.onValueChanged.AddListener(value =>
			{
				label.text = _title + " : " + value.ToString(_format);
			});
		}

		public void Setup(string title, float defaultValue, float min, float max, string format, bool wholeNumbers, UnityAction<float> OnValueChanged = null)
		{
			_title = title;
			slider.minValue = min;
			slider.maxValue = max;
			slider.value = defaultValue;
			_format = format;
			slider.wholeNumbers = wholeNumbers;
			label.text = title + " : " + defaultValue.ToString(format);
			slider.onValueChanged.AddListener(OnValueChanged);
		}
	}
}