using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Measure.Scripts.XR.Consoles.ControllerConsoles
{
	public class ToggleItem : MonoBehaviour
	{
		public TextMeshProUGUI label;
		public Toggle toggle;

		public void Setup(string title, bool defaultValue, UnityAction<bool> OnValueChanged)
		{
			label.text = title;
			toggle.isOn = defaultValue;
			toggle.onValueChanged.AddListener(OnValueChanged);
		}
	}
}