using Measure.Scripts.XR.Consoles.ControllerConsoles;
using TMPro;
using UnityEngine;

namespace Measure.Scripts.XR.Consoles
{
	public class RenderSettingConsole : MonoBehaviour
	{
		[Header("Prefabs")]
		[SerializeField] private NumberItem _numberItemPrefab;
		[SerializeField] private ToggleItem _toggleItemPrefab;
		[SerializeField] private EnumItem _enumItemPrefab;

		public void Show()
		{
			gameObject.SetActive(true);
		}

		public void Hide()
		{
			gameObject.SetActive(false);
		}

		private void Start()
		{
			Instantiate(_enumItemPrefab, transform).Setup("Rendering Mode", XRPlayer.Instance.renderingMode, value => XRPlayer.Instance.renderingMode = value);
			Instantiate(_enumItemPrefab, transform).Setup("Depth Priming Mode", XRPlayer.Instance.depthPrimingMode, value => XRPlayer.Instance.depthPrimingMode = value);
			Instantiate(_enumItemPrefab, transform).Setup("Copy Depth Mode", XRPlayer.Instance.copyDepthMode, value => XRPlayer.Instance.copyDepthMode = value);
			Instantiate(_toggleItemPrefab, transform).Setup("Use Native Renderpass", XRPlayer.Instance.useNativeRenderpass, value => XRPlayer.Instance.useNativeRenderpass = value);
			Instantiate(_enumItemPrefab,  transform).Setup("Intermediate Texture Mode", XRPlayer.Instance.intermediateTextureMode, value=> XRPlayer.Instance.intermediateTextureMode = value);
			Instantiate(_toggleItemPrefab, transform).Setup("Renderer Feature", XRPlayer.Instance.rendererFeature, value => XRPlayer.Instance.rendererFeature = value);
			Instantiate(_toggleItemPrefab, transform).Setup("Depth Texture", XRPlayer.Instance.depthTexture, value => XRPlayer.Instance.depthTexture = value);
			Instantiate(_toggleItemPrefab, transform).Setup("Opaque Texture", XRPlayer.Instance.opaqueTexture, value => XRPlayer.Instance.opaqueTexture = value);
			Instantiate(_toggleItemPrefab, transform).Setup("Shadow", XRPlayer.Instance.shadow, value => XRPlayer.Instance.shadow = value);
			Instantiate(_toggleItemPrefab, transform).Setup("Skybox", XRPlayer.Instance.skybox, value => XRPlayer.Instance.skybox = value);
			Instantiate(_toggleItemPrefab, transform).Setup("Post Processing", XRPlayer.Instance.postProcessing, value => XRPlayer.Instance.postProcessing = value);
			Instantiate(_toggleItemPrefab, transform).Setup("HDR", XRPlayer.Instance.hdr, value => XRPlayer.Instance.hdr = value);
			Instantiate(_toggleItemPrefab, transform).Setup("Bloom", XRPlayer.Instance.bloom, value => XRPlayer.Instance.bloom = value);
			Instantiate(_numberItemPrefab, transform).Setup("Bloom Threshold", XRPlayer.Instance.bloomThreshold, 0f, 3f, "F1", false, value => XRPlayer.Instance.bloomThreshold = value);
			Instantiate(_numberItemPrefab, transform).Setup("Bloom Intensity", XRPlayer.Instance.bloomIntensity, 1f, 20f, "F1", false, value => XRPlayer.Instance.bloomIntensity = value);
			Instantiate(_numberItemPrefab, transform).Setup("Bloom Scatter", XRPlayer.Instance.bloomScatter, 0f, 1f, "F1", false, value => XRPlayer.Instance.bloomScatter = value);
			Instantiate(_numberItemPrefab, transform).Setup("Bloom Max Iterations", XRPlayer.Instance.bloomMaxIteration, 1f, 16f, "F0", true, value => XRPlayer.Instance.bloomMaxIteration = (int) value);
			Instantiate(_enumItemPrefab, transform).Setup("Anti-aliasing Type", XRPlayer.Instance.antialiasingType, value => XRPlayer.Instance.antialiasingType = value);
			Instantiate(_enumItemPrefab, transform).Setup("MSAA", XRPlayer.Instance.msaa, value => XRPlayer.Instance.msaa = value);
			Instantiate(_enumItemPrefab, transform).Setup("FFR Level", XRPlayer.Instance.ffrLevel, value => XRPlayer.Instance.ffrLevel = value);
			Instantiate(_enumItemPrefab, transform).Setup("CPU Level", XRPlayer.Instance.cpuLevel, value => XRPlayer.Instance.cpuLevel = value);
			Instantiate(_enumItemPrefab, transform).Setup("GPU Level", XRPlayer.Instance.gpuLevel, value => XRPlayer.Instance.gpuLevel = value);

			RenderScale renderScaleDefault = RenderScale.Tier3;
			renderScaleDefault = XRPlayer.Instance.renderScale switch
			{
				0.6f => RenderScale.Tier1,
				0.8f => RenderScale.Tier2,
				1.0f => RenderScale.Tier3,
				1.2f => RenderScale.Tier4,
				1.5f => RenderScale.Tier5,
				2.0f => RenderScale.Tier6,
				_ => RenderScale.Tier3
			};
			var renderScale = Instantiate(_enumItemPrefab, transform);
			renderScale.Setup("Render Scale", renderScaleDefault, value =>
			{
				XRPlayer.Instance.renderScale = value switch
				{
					RenderScale.Tier1 => 0.6f,
					RenderScale.Tier2 => 0.8f,
					RenderScale.Tier3 => 1.0f,
					RenderScale.Tier4 => 1.2f,
					RenderScale.Tier5 => 1.5f,
					RenderScale.Tier6 => 2.0f,
					_ => 1.0f
				};
			});
			renderScale.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "0.6";
			renderScale.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "0.8";
			renderScale.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = "1.0";
			renderScale.transform.GetChild(3).GetComponent<TextMeshProUGUI>().text = "1.2";
			renderScale.transform.GetChild(4).GetComponent<TextMeshProUGUI>().text = "1.5";
			renderScale.transform.GetChild(5).GetComponent<TextMeshProUGUI>().text = "2.0";
		}

		private enum RenderScale
		{
			Tier1,
			Tier2,
			Tier3,
			Tier4,
			Tier5,
			Tier6,
		}
	}
}