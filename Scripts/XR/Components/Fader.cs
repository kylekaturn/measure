using DG.Tweening;
using UnityEngine;

namespace Measure.Scripts.XR.Components
{
	public class Fader : MonoBehaviour
	{
		public SpriteRenderer spriteRenderer;

		private void Awake()
		{
			spriteRenderer.enabled = false;
		}

		public void FadeIn(float duration = 0.5f)
		{
			spriteRenderer.enabled = true;
			spriteRenderer.DOFade(1f, 0f);
			spriteRenderer.DOFade(0f, duration).SetEase(Ease.Linear).OnComplete(() =>
			{
				spriteRenderer.enabled = false;
			});
		}

		public void FadeOut(float duration = 0.5f)
		{
			spriteRenderer.enabled = true;
			spriteRenderer.DOFade(0f, 0f);
			spriteRenderer.DOFade(1f, duration).SetEase(Ease.Linear);
		}
	}
	
	
}
