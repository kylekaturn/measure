using Measure.Scripts.Attributes;
using Measure.Scripts.Extensions;
using UnityEngine;

namespace Measure.Scripts.XR.Components
{
	public class PlayerFollower : MonoBehaviour
	{
		public enum FollowingType { PositionOnly, LookBody, LookHead }

		[Separator("Following Type")]
		public FollowingType followingType = FollowingType.PositionOnly;

		[Space(10)]
		[Separator("Smooth Following")]
		public bool smoothFollowing = true;
		[Conditional(nameof(smoothFollowing))]
		public float followingSpeed = 5f;

		[Space(10)]
		[Separator("Override Position")]
		public bool overridePosition = false;
		[Conditional(nameof(overridePosition))]
		public Vector3 position;

		[Separator("Update")]
		public bool autoUpdate = true;

		private void Awake()
		{
			if (!overridePosition) position = transform.position;
		}

		private void LateUpdate()
		{
			if (!autoUpdate) return;
			if (XRPlayer.Instance == null) return;
			UpdatePosition();
		}

		public void UpdatePosition()
		{
			float amount = (smoothFollowing && autoUpdate) ? Time.deltaTime * followingSpeed : 1f;
			Quaternion lookRotation;

			switch (followingType)
			{
				case FollowingType.PositionOnly:
					transform.position = Vector3.Lerp(transform.position, XRPlayer.Instance.transform.position + position, amount);
					break;

				case FollowingType.LookBody:
					transform.position = Vector3.Lerp(transform.position, XRPlayer.Instance.transform.TransformPoint(position), amount);
					lookRotation = Quaternion.LookRotation(transform.position.Floor() - XRPlayer.Instance.transform.position);
					transform.rotation = Quaternion.Lerp(transform.rotation, lookRotation, amount);
					break;

				case FollowingType.LookHead:
					transform.position = Vector3.Lerp(transform.position, XRPlayer.Instance.headset.transform.TransformPoint(position), amount);
					lookRotation = Quaternion.LookRotation(transform.position - XRPlayer.Instance.headset.transform.position);
					transform.rotation = Quaternion.Lerp(transform.rotation, lookRotation, amount);
					break;
			}
		}
	}
}