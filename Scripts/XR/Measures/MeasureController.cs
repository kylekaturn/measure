using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DG.Tweening;
using Measure.Scripts.Extensions;
using Measure.Scripts.Internal;
using Measure.Scripts.XR.Consoles;
using Measure.Scripts.XR.Measures.Data;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace Measure.Scripts.XR.Measures
{
	public class MeasureController : MonoBehaviour
	{
		[Header("Configuration")]
		public bool autoStart = false;
		[Range(1, 10)]
		public int duration = 5;
		public string comment = "";
		public bool uploadResult = true;
		public bool saveJsonOnEditor = false;

		[Header("Components")]
		public GameObject floor;
		public GameObject startPanel;
		public TextMeshProUGUI titleText;
		public Toggle autoStartToggle;
		public TextMeshProUGUI durationLabel;
		public Slider durationSlider;
		public Button startButton;
		public TextMeshProUGUI startButtonText;
		public TextMeshProUGUI infoText;
		public TextMeshProUGUI informationText;
		public AudioSource fanfareSound;

		private MeasureData measureData;
		private MeasureItem item;
		private int gpuTime = 0;
		private int blankGpuTime = 0;
		private Texture2D uploadTexture;

		private Tween a;

		private void Start()
		{
			try
			{
				item = FindObjectsByType<MeasureItem>(FindObjectsInactive.Exclude, FindObjectsSortMode.None).ToList()[0];
			}
			catch
			{
				return;
			}

			item.gameObject.SetActive(false);

			autoStartToggle.isOn = autoStart;
			titleText.text = item.name;

			durationSlider.onValueChanged.AddListener(value =>
			{
				durationLabel.text = "Duration (Seconds) : " + value.ToInt();
				duration = value.ToInt();
			});
			startButton.onClick.AddListener(StartButtonClicked);

			if (autoStart)
			{
				a = DOVirtual.DelayedCall(5.0f, StartButtonClicked, false).OnUpdate(() =>
				{
					startButtonText.text = $"Start in {(5.0f - a.position):F0}";
				}).OnComplete(() =>
				{
					startButtonText.text = "Start";
				});
			}
		}

		private void StartGPUTimer()
		{
			DelayedCaller.KillOf(this);
			gpuTime = XRPlayer.Instance.appGPUTime;
			DelayedCaller.DelayedCall(this, 0.1f, () =>
			{
				gpuTime = (int) Mathf.Lerp(gpuTime, XRPlayer.Instance.appGPUTime, 0.2f);
			}, true, true);
		}

		private int GetGPUTime()
		{
			return gpuTime;
		}

		private void Update()
		{
			if (item == null) return;
			infoText.text = $@"Total Measure : {item.GetItemsCount()}
Total Duration : {item.GetItemsCount() * duration} Seconds";
			durationSlider.value = duration;
		}

		private void Reset()
		{
			autoStart = false;
			item.gameObject.SetActive(false);
			startPanel.gameObject.SetActive(true);
			floor.SetActive(true);
			FindFirstObjectByType<InputSimulator>(FindObjectsInactive.Include)?.gameObject.SetActive(true);
			XRPlayer.Instance.headset.ResumeTracking();
			XRPlayer.Instance.headset.enabled = true;
			XRPlayer.Instance.consoleLevel = ConsoleHolder.Level.RenderSettingOnly;
			XRPlayer.Instance.showQuestController = true;
			XRPlayer.Instance.uiInteractorEnabled = true;
			informationText.text = "";
		}

		private void StartButtonClicked()
		{
			StartCoroutine(StartMeasure());
		}

		private IEnumerator StartMeasure()
		{
			FindFirstObjectByType<InputSimulator>()?.gameObject.SetActive(false);
			XRPlayer.Instance.headset.PauseTracking();
			XRPlayer.Instance.headset.enabled = false;
			XRPlayer.Instance.headset.transform.localPosition = new Vector3(0, 1.5f, 0f);
			XRPlayer.Instance.headset.transform.rotation = Quaternion.identity;
			XRPlayer.Instance.consoleLevel = ConsoleHolder.Level.None;
			XRPlayer.Instance.showQuestController = false;
			XRPlayer.Instance.uiInteractorEnabled = false;

			startPanel.gameObject.SetActive(false);
			floor.SetActive(false);

			float time = 0f;
			StartGPUTimer();

			while (time < duration && !Application.isEditor)
			{
				informationText.text = $@"
Performance Measuring
Measuring Blank Scene
Progress : {time:F1} / {duration}
GPU Time : <size=80><color=#00FF00>{GetGPUTime():F0}
";
				time += Time.deltaTime;
				yield return new WaitForEndOfFrame();
			}
			blankGpuTime = GetGPUTime();

			measureData = new MeasureData();
			measureData.name = item.name;
			measureData.version = Application.version;
			measureData.device = SystemInfo.deviceName;
			measureData.unityVersion = Application.unityVersion;
			measureData.graphicsAPI = SystemInfo.graphicsDeviceType.ToString();
			measureData.blankGPUTime = blankGpuTime;
			measureData.eyeTextureWidth = XRPlayer.Instance.eyeTextureSize.x;
			measureData.eyeTextureHeight = XRPlayer.Instance.eyeTextureSize.y;
			measureData.renderConfiguration = XRPlayer.Instance.xrConfiguration;
			measureData.comment = comment;
			measureData.time = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
			measureData.measureResults = new List<MeasureResult>();

			for (int i = 0; i < item.GetItemsCount(); i++)
			{
				item.gameObject.SetActive(true);
				item.SetItem(i);
				informationText.text = "";
				time = 0f;
				StartGPUTimer();
				while (time < duration)
				{
					informationText.text = $@"
Object : {item.name}
Level : {i + 1} / {item.GetItemsCount()}
Duplicates : {item.GetDuplicateCount()}
Triangles : {item.GetTrianglesCount()}
Description : {item.GetDescription()}
Progress : {time:F1} / {duration}
GPU Time Total : {GetGPUTime()}
GPU Time Self : <size=80><color=#00FF00>{(gpuTime - blankGpuTime):F0}</color>
";
					time += Time.deltaTime;
					yield return new WaitForEndOfFrame();
				}
				measureData.measureResults.Add(new MeasureResult()
				{
					name = item.GetDescription(),
					count = item.GetDuplicateCount(),
					triangles = item.GetTrianglesCount(),
					selfGPUTime = GetGPUTime() - blankGpuTime,
					totalGPUTime = GetGPUTime()
				});
				uploadTexture = ScreenCapture.CaptureScreenshotAsTexture();
			}
			item.ResetItem();
			item.gameObject.SetActive(false);

			string rootPath = Application.persistentDataPath;
			string jsonString = JsonUtility.ToJson(measureData, true);

			switch (XRPlayer.platform)
			{
				case XRPlatform.Device:
					//File.WriteAllText($"{rootPath}/{item.name}.json", jsonString);
					if (uploadResult) StartCoroutine(Upload(jsonString));
					break;
				case XRPlatform.Editor when saveJsonOnEditor:
					File.WriteAllText($"{item.name}.json", jsonString);
					break;
			}
			fanfareSound.Play();
			Reset();
		}

		private IEnumerator Upload(string jsonString)
		{
			List<IMultipartFormSection> formData = new List<IMultipartFormSection>();

			Debug.Log(uploadTexture.width + " : " + uploadTexture.height);

			uploadTexture = SystemInfo.deviceName switch
			{
				"Quest 2" => uploadTexture.Crop(330, 0, 330, 0),
				"Quest 3" => uploadTexture.Crop(900, 650, 700, 0),
				_ => uploadTexture.Crop(900, 650, 700, 0)
			};
			uploadTexture = uploadTexture.ChangeSize(uploadTexture.width / 2, uploadTexture.height / 2);

			byte[] textureBytes = uploadTexture.EncodeToJPG();
			formData.Add(new MultipartFormFileSection("texture", textureBytes, "texture.png", "image/jpg"));

			byte[] jsonBytes = new System.Text.UTF8Encoding().GetBytes(jsonString);
			formData.Add(new MultipartFormDataSection("json", jsonString));

			UnityWebRequest request = UnityWebRequest.Post("https://admin.katurn.com/api/vibepunch/measure/upload", formData);
			//UnityWebRequest request = UnityWebRequest.Post("http://localhost:5001/api/vibepunch/measure/upload", formData);
			request.downloadHandler = new DownloadHandlerBuffer();

			yield return request.SendWebRequest();

			Debug.Log(request.result == UnityWebRequest.Result.Success ? request.downloadHandler.text : request.error);
		}
	}
}