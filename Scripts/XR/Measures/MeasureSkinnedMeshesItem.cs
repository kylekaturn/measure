using System.Collections.Generic;
using System.Linq;
using Measure.Scripts.Extensions;
using UnityEngine;

namespace Measure.Scripts.XR.Measures
{
	public class MeasureSkinnedMeshesItem : MeasureItem
	{
		public List<SkinnedMeshRenderer> skinnedMeshRenderers;
		public List<Material> materials;

		public override void SetItem(int index)
		{
			if (materials.Count > 0) skinnedMeshRenderers.ForEach(x => x.SetMaterial(materials[index]));
		}

		public override int GetDuplicateCount()
		{
			return 1;
		}

		public override int GetItemsCount()
		{
			return Mathf.Max(1, materials.Count);
		}

		public override string GetDescription()
		{
			return skinnedMeshRenderers[0].sharedMaterial.name;
		}

		public override int GetTrianglesCount()
		{
			return  skinnedMeshRenderers.Sum(filter => filter.sharedMesh.triangles.Length / 3);
		}
	}
}