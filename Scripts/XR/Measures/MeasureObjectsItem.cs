using System.Collections.Generic;
using System.Linq;
using Measure.Scripts.Extensions;
using UnityEngine;

namespace Measure.Scripts.XR.Measures
{
	public class MeasureObjectsItem : MeasureItem
	{
		private List<GameObject> objects;
		private int index = 0;

		private void Awake()
		{
			objects = new List<GameObject>();
			transform.GetChildren().ToList().ForEach(x =>
			{
				if (x.gameObject.activeSelf) objects.Add(x.gameObject);
			});
		}

		public override void ResetItem()
		{
			objects.ForEach(x => x.SetActive(false));
		}

		public override void SetItem(int index)
		{
			this.index = index;
			objects.ForEach(x => x.SetActive(false));
			objects[index].SetActive(true);
		}

		public override int GetDuplicateCount()
		{
			return objects[index].GetChildren().Count;
		}

		public override int GetItemsCount()
		{
			return objects.Count;
		}

		public override string GetDescription()
		{
			return objects[index].name;
		}

		public override int GetTrianglesCount()
		{
			return 0;
		}
	}
}