using System.Collections.Generic;
using UnityEngine;

namespace Measure.Scripts.XR.Measures
{
	public class MeasureSkinnedMeshItem : MeasureItem
	{
		public SkinnedMeshRenderer skinnedMeshRenderer;
		public List<Material> materials;

		public override int GetItemsCount()
		{
			return 1;
            //throw new System.NotImplementedException();
		}

		public override void SetItem(int index)
		{
			//throw new System.NotImplementedException();
		}

		public override int GetDuplicateCount()
		{
			return 1;
			//throw new System.NotImplementedException();
		}

		public override string GetDescription()
		{
			return "";
			//throw new System.NotImplementedException();
		}

		public override int GetTrianglesCount()
		{
			return 0;
			//throw new System.NotImplementedException();
		}
	}
}