using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.XR.Interaction.Toolkit.Inputs;
using UnityEngine.XR.Interaction.Toolkit.UI;

namespace Measure.Scripts.XR
{
	public class XRUtils : MonoBehaviour
	{
		public XRInteractionManager xrInteractionManager;
		public XRUIInputModule xruiInputModule;
		public InputActionManager inputActionManager;
		public InputSimulator inputSimulator;
	}
}