namespace Measure.Scripts.XR
{
	public enum XRPlatform
	{
		Editor,
		OculusLink,
		Device
	}
}