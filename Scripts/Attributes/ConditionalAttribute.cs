﻿using System;
using UnityEditor;
using UnityEngine;

namespace Measure.Scripts.Attributes
{
	[AttributeUsage(AttributeTargets.Field)]
	public class ConditionalAttribute : PropertyAttribute
	{
		public string conditionalSourceField;
		public string compareString;
		public bool hideInInspector = false;
		public bool inverse = false;

		public ConditionalAttribute(string conditionalSourceField, bool hideInInspector = false, bool inverse = false)
		{
			this.conditionalSourceField = conditionalSourceField;
			this.hideInInspector = hideInInspector;
			this.inverse = inverse;
		}

		public ConditionalAttribute(string conditionalSourceField, string compareString, bool hideInInspector = false, bool inverse = false)
		{
			this.conditionalSourceField = conditionalSourceField;
			this.compareString = compareString;
			this.hideInInspector = hideInInspector;
			this.inverse = inverse;
		}
		
#if UNITY_EDITOR
		public static bool GetConditionalHideAttributeResult(ConditionalAttribute conditionalAttribute, SerializedProperty property)
		{
			bool enabled = true;
			
			SerializedProperty sourcePropertyValue;
			if (!property.isArray)
			{
				string propertyPath = property.propertyPath;
				string conditionPath = propertyPath.Replace(property.name, conditionalAttribute.conditionalSourceField);
				sourcePropertyValue = property.serializedObject.FindProperty(conditionPath);
				if (sourcePropertyValue == null)
				{
					sourcePropertyValue = property.serializedObject.FindProperty(conditionalAttribute.conditionalSourceField);
				}
			}
			else
			{
				sourcePropertyValue = property.serializedObject.FindProperty(conditionalAttribute.conditionalSourceField);
			}
			
			if (sourcePropertyValue != null)
			{
				enabled = CheckPropertyType(sourcePropertyValue, conditionalAttribute.compareString);
			}

			if (conditionalAttribute.inverse) enabled = !enabled;

			return enabled;
		}
		
		public static bool CheckPropertyType(SerializedProperty sourcePropertyValue, string compareString = "")
		{
			switch (sourcePropertyValue.propertyType)
			{
				case SerializedPropertyType.Boolean:
					return sourcePropertyValue.boolValue;
				case SerializedPropertyType.ObjectReference:
					return sourcePropertyValue.objectReferenceValue != null;
				case SerializedPropertyType.Enum:
					return sourcePropertyValue.enumNames[sourcePropertyValue.enumValueIndex] == compareString;
				default:
					Debug.LogError("Data type of the property used for conditional hiding [" + sourcePropertyValue.propertyType + "] is currently not supported");
					return true;
			}
		}
#endif
	}
}